package org.przedszkolaki.squat.repository;

import org.przedszkolaki.squat.domain.Payslip;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Payslip entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PayslipRepository extends JpaRepository<Payslip, Long> {

}
