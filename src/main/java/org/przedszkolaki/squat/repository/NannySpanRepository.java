package org.przedszkolaki.squat.repository;

import org.przedszkolaki.squat.domain.NannySpan;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NannySpan entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NannySpanRepository extends JpaRepository<NannySpan, Long> {

}
