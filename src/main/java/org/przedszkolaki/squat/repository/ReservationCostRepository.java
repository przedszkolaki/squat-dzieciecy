package org.przedszkolaki.squat.repository;

import org.przedszkolaki.squat.domain.ReservationCost;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ReservationCost entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReservationCostRepository extends JpaRepository<ReservationCost, Long> {

}
