package org.przedszkolaki.squat.repository;

import java.util.List;

import org.przedszkolaki.squat.domain.Baby;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Baby entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BabyRepository extends JpaRepository<Baby, Long> {

	@Query("SELECT b FROM Baby b WHERE b.parent.user.login = :login")
	List<Baby> findByUserLogin(@Param("login") String login);

}
