package org.przedszkolaki.squat.repository;

import java.util.List;

import org.przedszkolaki.squat.domain.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Reservation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

	@Query("SELECT r FROM Reservation r WHERE r.baby.parent.user.login = :login")
	List<Reservation> findByUserLogin(@Param("login") String login);

}
