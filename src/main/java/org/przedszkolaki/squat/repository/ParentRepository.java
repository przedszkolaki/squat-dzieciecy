package org.przedszkolaki.squat.repository;

import java.util.Optional;

import org.przedszkolaki.squat.domain.Parent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the Parent entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParentRepository extends JpaRepository<Parent, Long> {

	@Query("SELECT p FROM Parent p WHERE p.user.login = :login")
	Optional<Parent> findByUserLogin(@Param("login") String login);

}
