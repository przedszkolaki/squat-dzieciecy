package org.przedszkolaki.squat.repository;

import java.time.LocalDate;
import java.util.List;

import org.przedszkolaki.squat.domain.BillingCycle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the BillingCycle entity.
 */
@Repository
public interface BillingCycleRepository extends JpaRepository<BillingCycle, Long> {

	@Query("SELECT bc FROM BillingCycle bc WHERE :startDate BETWEEN bc.startDate AND bc.endDate"
			+ " OR :endDate BETWEEN bc.startDate AND bc.endDate"
			+ " OR :startDate <= bc.startDate AND bc.endDate <= :endDate")
	List<BillingCycle> findCyclesOverlappingGivenDates(@Param("startDate") LocalDate startDate,
			@Param("endDate") LocalDate endDate);
	
}
