package org.przedszkolaki.squat.repository;

import java.util.List;

import org.przedszkolaki.squat.domain.Day;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Day entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DayRepository extends JpaRepository<Day, Long> {

	@Query("SELECT d FROM Day d WHERE d.billingCycle.id = :id ORDER BY d.date ASC")
	List<Day> findAllByBillingCycleId(@Param("id") Long id);

}
