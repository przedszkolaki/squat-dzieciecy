package org.przedszkolaki.squat.repository;

import org.przedszkolaki.squat.domain.Nanny;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Nanny entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NannyRepository extends JpaRepository<Nanny, Long> {

}
