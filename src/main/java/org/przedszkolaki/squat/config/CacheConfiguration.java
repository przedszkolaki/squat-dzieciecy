package org.przedszkolaki.squat.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import io.github.jhipster.config.jcache.BeanClassLoaderAwareJCacheRegionFactory;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        BeanClassLoaderAwareJCacheRegionFactory.setBeanClassLoader(this.getClass().getClassLoader());
        JHipsterProperties.Cache.Ehcache ehcache =
            jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            cm.createCache(org.przedszkolaki.squat.repository.UserRepository.USERS_BY_LOGIN_CACHE, jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.repository.UserRepository.USERS_BY_EMAIL_CACHE, jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.User.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Authority.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.User.class.getName() + ".authorities", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Baby.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Baby.class.getName() + ".reservations", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Reservation.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Reservation.class.getName() + ".reservationCosts", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Day.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Day.class.getName() + ".nannySpans", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Day.class.getName() + ".reservations", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.NannySpan.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.BillingCycle.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.BillingCycle.class.getName() + ".days", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.BillingCycle.class.getName() + ".invoices", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.BillingCycle.class.getName() + ".payslips", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.ReservationCost.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Invoice.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Nanny.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Nanny.class.getName() + ".nannySpans", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Nanny.class.getName() + ".payslips", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Payslip.class.getName(), jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Nanny.class.getName() + ".paySlips", jcacheConfiguration);
            cm.createCache(org.przedszkolaki.squat.domain.Parent.class.getName(), jcacheConfiguration);
            // jhipster-needle-ehcache-add-entry
        };
    }
}
