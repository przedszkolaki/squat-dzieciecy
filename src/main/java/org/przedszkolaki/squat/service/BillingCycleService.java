package org.przedszkolaki.squat.service;

import org.przedszkolaki.squat.domain.BillingCycle;
import org.przedszkolaki.squat.repository.BillingCycleRepository;
import org.przedszkolaki.squat.repository.DayRepository;
import org.przedszkolaki.squat.web.rest.vm.BillingCycleWithDaysVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class BillingCycleService {
	private final Logger log = LoggerFactory.getLogger(BillingCycleService.class);

	private final BillingCycleRepository billingCycleRepository;
	private final DayRepository dayRepository;

	public BillingCycleService(BillingCycleRepository billingCycleRepository, DayRepository dayRepository) {
		super();
		this.billingCycleRepository = billingCycleRepository;
		this.dayRepository = dayRepository;
	}

	public BillingCycle createBillingCycle(BillingCycleWithDaysVM billingCycleWithDaysVM) {
		log.debug("saving BillingCycleWithDays : {}", billingCycleWithDaysVM);
		BillingCycle billingCycle = billingCycleRepository.save(billingCycleWithDaysVM.getBillingCycle());
		billingCycleWithDaysVM.getDays().forEach(day -> {
			day.setBillingCycle(billingCycle);
			dayRepository.save(day);
		});
		return billingCycle;
	}

	public void deleteBillingCycle(Long id) {
		log.debug("deleting BillingCycle : {}", id);
		dayRepository.deleteAll(dayRepository.findAllByBillingCycleId(id));
		billingCycleRepository.deleteById(id);
	}

}
