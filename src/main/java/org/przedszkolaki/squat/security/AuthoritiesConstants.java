package org.przedszkolaki.squat.security;

/**
 * Constants for Spring Security authorities.
 */
public final class AuthoritiesConstants {

    public static final String ADMIN = "ROLE_ADMIN";

    public static final String USER = "ROLE_USER";

    public static final String ANONYMOUS = "ROLE_ANONYMOUS";

    public static final String COORDINATOR = "ROLE_COORDINATOR";

    public static final String PARENT = "ROLE_PARENT";
    
    public static final String NANNY = "ROLE_NANNY";

    private AuthoritiesConstants() {
    }
}
