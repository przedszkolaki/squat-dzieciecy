/**
 * View Models used by Spring MVC REST controllers.
 */
package org.przedszkolaki.squat.web.rest.vm;
