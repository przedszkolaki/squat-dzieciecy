package org.przedszkolaki.squat.web.rest.vm;

import java.util.List;

import org.przedszkolaki.squat.domain.BillingCycle;
import org.przedszkolaki.squat.domain.Day;

/**
 * View Model object for creating billing cycle together with days.
 */
public class BillingCycleWithDaysVM {

	private BillingCycle billingCycle;

	private List<Day> days;

	public BillingCycle getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(BillingCycle billingCycle) {
		this.billingCycle = billingCycle;
	}

	public List<Day> getDays() {
		return days;
	}

	public void setDays(List<Day> days) {
		this.days = days;
	}

}
