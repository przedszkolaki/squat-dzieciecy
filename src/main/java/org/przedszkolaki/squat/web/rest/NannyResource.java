package org.przedszkolaki.squat.web.rest;
import org.przedszkolaki.squat.domain.Nanny;
import org.przedszkolaki.squat.repository.NannyRepository;
import org.przedszkolaki.squat.web.rest.errors.BadRequestAlertException;
import org.przedszkolaki.squat.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Nanny.
 */
@RestController
@RequestMapping("/api")
public class NannyResource {

    private final Logger log = LoggerFactory.getLogger(NannyResource.class);

    private static final String ENTITY_NAME = "nanny";

    private final NannyRepository nannyRepository;

    public NannyResource(NannyRepository nannyRepository) {
        this.nannyRepository = nannyRepository;
    }

    /**
     * POST  /nannies : Create a new nanny.
     *
     * @param nanny the nanny to create
     * @return the ResponseEntity with status 201 (Created) and with body the new nanny, or with status 400 (Bad Request) if the nanny has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/nannies")
    public ResponseEntity<Nanny> createNanny(@Valid @RequestBody Nanny nanny) throws URISyntaxException {
        log.debug("REST request to save Nanny : {}", nanny);
        if (nanny.getId() != null) {
            throw new BadRequestAlertException("A new nanny cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Nanny result = nannyRepository.save(nanny);
        return ResponseEntity.created(new URI("/api/nannies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /nannies : Updates an existing nanny.
     *
     * @param nanny the nanny to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated nanny,
     * or with status 400 (Bad Request) if the nanny is not valid,
     * or with status 500 (Internal Server Error) if the nanny couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/nannies")
    public ResponseEntity<Nanny> updateNanny(@Valid @RequestBody Nanny nanny) throws URISyntaxException {
        log.debug("REST request to update Nanny : {}", nanny);
        if (nanny.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Nanny result = nannyRepository.save(nanny);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, nanny.getId().toString()))
            .body(result);
    }

    /**
     * GET  /nannies : get all the nannies.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of nannies in body
     */
    @GetMapping("/nannies")
    public List<Nanny> getAllNannies() {
        log.debug("REST request to get all Nannies");
        return nannyRepository.findAll();
    }

    /**
     * GET  /nannies/:id : get the "id" nanny.
     *
     * @param id the id of the nanny to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the nanny, or with status 404 (Not Found)
     */
    @GetMapping("/nannies/{id}")
    public ResponseEntity<Nanny> getNanny(@PathVariable Long id) {
        log.debug("REST request to get Nanny : {}", id);
        Optional<Nanny> nanny = nannyRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(nanny);
    }

    /**
     * DELETE  /nannies/:id : delete the "id" nanny.
     *
     * @param id the id of the nanny to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/nannies/{id}")
    public ResponseEntity<Void> deleteNanny(@PathVariable Long id) {
        log.debug("REST request to delete Nanny : {}", id);
        nannyRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
