package org.przedszkolaki.squat.web.rest;

import static com.google.common.base.Preconditions.checkArgument;
import static org.przedszkolaki.squat.security.SecurityUtils.getCurrentUserLogin;
import static org.przedszkolaki.squat.security.SecurityUtils.isCurrentUserInRole;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.przedszkolaki.squat.domain.Baby;
import org.przedszkolaki.squat.domain.Parent;
import org.przedszkolaki.squat.repository.BabyRepository;
import org.przedszkolaki.squat.repository.ParentRepository;
import org.przedszkolaki.squat.security.AuthoritiesConstants;
import org.przedszkolaki.squat.security.SecurityUtils;
import org.przedszkolaki.squat.web.rest.errors.BadRequestAlertException;
import org.przedszkolaki.squat.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Baby.
 */
@RestController
@RequestMapping("/api")
public class BabyResource {

	private final Logger log = LoggerFactory.getLogger(BabyResource.class);

	private static final String ENTITY_NAME = "baby";

	private final BabyRepository babyRepository;
	private final ParentRepository parentRepository;

	public BabyResource(BabyRepository babyRepository, ParentRepository parentRepository) {
		this.babyRepository = babyRepository;
		this.parentRepository = parentRepository;
	}

	/**
	 * POST /babies : Create a new baby.
	 *
	 * @param baby the baby to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         baby, or with status 400 (Bad Request) if the baby has already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@Secured(AuthoritiesConstants.PARENT)
	@PostMapping("/babies")
	public ResponseEntity<Baby> createBaby(@Valid @RequestBody Baby baby) throws URISyntaxException {
		log.debug("REST request to save Baby : {}", baby);
		if (baby.getId() != null) {
			throw new BadRequestAlertException("A new baby cannot already have an ID", ENTITY_NAME, "idexists");
		}
		setCurrentUserAsParent(baby);
		Baby result = babyRepository.save(baby);
		return ResponseEntity.created(new URI("/api/babies/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /babies : Updates an existing baby.
	 *
	 * @param baby the baby to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         baby, or with status 400 (Bad Request) if the baby is not valid, or
	 *         with status 500 (Internal Server Error) if the baby couldn't be
	 *         updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@Secured({ AuthoritiesConstants.PARENT, AuthoritiesConstants.COORDINATOR })
	@PutMapping("/babies")
	public ResponseEntity<Baby> updateBaby(@Valid @RequestBody Baby baby) throws URISyntaxException {
		log.debug("REST request to update Baby : {}", baby);
		if (baby.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		if (!isCurrentUserInRole(AuthoritiesConstants.COORDINATOR)) {
			Baby managedBaby = babyRepository.findById(baby.getId()).get();
			checkArgument(managedBaby.getParent().getUser().getLogin().equals(getCurrentUserLogin().get()),
					"baby must belong to the current user");
			checkArgument(managedBaby.getParent().getId() == baby.getParent().getId(),
					"baby must not change ownership of a baby");
		}

		Baby result = babyRepository.save(baby);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, baby.getId().toString()))
				.body(result);
	}

	/**
	 * GET /babies : get all the babies.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of babies in
	 *         body
	 */
	@GetMapping("/babies")
	public List<Baby> getAllBabies() {
		log.debug("REST request to get all Babies");
		return babyRepository.findAll();
	}

	/**
	 * GET /babies : get all the babies.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of babies in
	 *         body
	 */
	@GetMapping("/babies/forCurrentUser")
	public List<Baby> getBabiesForCurrentUser() {
		log.debug("REST request to get all Babies");
		String login = SecurityUtils.getCurrentUserLogin().get();
		return babyRepository.findByUserLogin(login);
	}

	/**
	 * GET /babies/:id : get the "id" baby.
	 *
	 * @param id the id of the baby to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the baby, or
	 *         with status 404 (Not Found)
	 */
	@GetMapping("/babies/{id}")
	public ResponseEntity<Baby> getBaby(@PathVariable Long id) {
		log.debug("REST request to get Baby : {}", id);
		Optional<Baby> baby = babyRepository.findById(id);
		return ResponseUtil.wrapOrNotFound(baby);
	}

	/**
	 * DELETE /babies/:id : delete the "id" baby.
	 *
	 * @param id the id of the baby to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@Secured({ AuthoritiesConstants.PARENT, AuthoritiesConstants.COORDINATOR })
	@DeleteMapping("/babies/{id}")
	public ResponseEntity<Void> deleteBaby(@PathVariable Long id) {
		log.debug("REST request to delete Baby : {}", id);
		if (!isCurrentUserInRole(AuthoritiesConstants.COORDINATOR)) {
			Baby baby = babyRepository.findById(id).get();
			checkArgument(baby.getParent().getUser().getLogin().equals(getCurrentUserLogin().get()),
					"baby must belong to the current user");
		}
		babyRepository.deleteById(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	private void setCurrentUserAsParent(Baby baby) {
		String login = SecurityUtils.getCurrentUserLogin()
				.orElseThrow(() -> new RuntimeException("current user login not found"));
		Parent parent = parentRepository.findByUserLogin(login)
				.orElseThrow(() -> new RuntimeException("parent entity for the current user not found"));
		baby.setParent(parent);
	}

}
