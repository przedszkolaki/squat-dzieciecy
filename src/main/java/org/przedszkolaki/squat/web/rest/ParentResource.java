package org.przedszkolaki.squat.web.rest;

import static org.przedszkolaki.squat.security.SecurityUtils.getCurrentUserLogin;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.przedszkolaki.squat.domain.Parent;
import org.przedszkolaki.squat.repository.ParentRepository;
import org.przedszkolaki.squat.security.AuthoritiesConstants;
import org.przedszkolaki.squat.service.UserService;
import org.przedszkolaki.squat.web.rest.errors.BadRequestAlertException;
import org.przedszkolaki.squat.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Parent.
 */
@Secured({ AuthoritiesConstants.COORDINATOR })
@RestController
@RequestMapping("/api")
public class ParentResource {

    private final Logger log = LoggerFactory.getLogger(ParentResource.class);

    private static final String ENTITY_NAME = "parent";

    private final ParentRepository parentRepository;
	private final UserService userService;

	public ParentResource(ParentRepository parentRepository, UserService userService) {
        this.parentRepository = parentRepository;
		this.userService = userService;
    }

    /**
     * POST  /parents : Create a new parent.
     *
     * @param parent the parent to create
     * @return the ResponseEntity with status 201 (Created) and with body the new parent, or with status 400 (Bad Request) if the parent has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/parents")
    public ResponseEntity<Parent> createParent(@Valid @RequestBody Parent parent) throws URISyntaxException {
        log.debug("REST request to save Parent : {}", parent);
        if (parent.getId() != null) {
            throw new BadRequestAlertException("A new parent cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Parent result = parentRepository.save(parent);
		userService.addAuthority(result.getUser().getId(), AuthoritiesConstants.PARENT);
        return ResponseEntity.created(new URI("/api/parents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * GET  /parents : get all the parents.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of parents in body
     */
	@Secured({ AuthoritiesConstants.USER })
    @GetMapping("/parents")
    public List<Parent> getAllParents() {
        log.debug("REST request to get all Parents");
        return parentRepository.findAll();
    }

    /**
     * GET  /parents/:id : get the "id" parent.
     *
     * @param id the id of the parent to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the parent, or with status 404 (Not Found)
     */
	@Secured({ AuthoritiesConstants.USER })
    @GetMapping("/parents/{id}")
    public ResponseEntity<Parent> getParent(@PathVariable Long id) {
        log.debug("REST request to get Parent : {}", id);
        Optional<Parent> parent = parentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(parent);
    }

    /**
	 * GET /parents/forCurrentUser : get the current user's parent object.
	 *
	 * @return the ResponseEntity with status 200 (OK) and with body the parent, or
	 *         with status 404 (Not Found)
	 */
	@Secured({ AuthoritiesConstants.PARENT })
	@GetMapping("/parents/forCurrentUser")
	public ResponseEntity<Parent> getParentForCurrentUser() {
		log.debug("REST request to get Parent for the current user");
		return ResponseUtil.wrapOrNotFound(
				getCurrentUserLogin().flatMap(parentRepository::findByUserLogin));
	}

	/**
	 * DELETE /parents/:id : delete the "id" parent.
	 *
	 * @param id the id of the parent to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
    @DeleteMapping("/parents/{id}")
    public ResponseEntity<Void> deleteParent(@PathVariable Long id) {
        log.debug("REST request to delete Parent : {}", id);
        parentRepository.findById(id).ifPresent(parent -> {;
			parentRepository.deleteById(id);
			userService.removeAuthority(parent.getUser().getId(), AuthoritiesConstants.PARENT);
		});
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
