package org.przedszkolaki.squat.web.rest;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.przedszkolaki.squat.domain.BillingCycle;
import org.przedszkolaki.squat.domain.Day;
import org.przedszkolaki.squat.repository.BillingCycleRepository;
import org.przedszkolaki.squat.repository.DayRepository;
import org.przedszkolaki.squat.security.AuthoritiesConstants;
import org.przedszkolaki.squat.service.BillingCycleService;
import org.przedszkolaki.squat.web.rest.errors.BadRequestAlertException;
import org.przedszkolaki.squat.web.rest.util.HeaderUtil;
import org.przedszkolaki.squat.web.rest.vm.BillingCycleWithDaysVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Preconditions;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing BillingCycle.
 */
@Secured({AuthoritiesConstants.COORDINATOR})
@RestController
@RequestMapping("/api")
public class BillingCycleResource {

    private final Logger log = LoggerFactory.getLogger(BillingCycleResource.class);

    private static final String ENTITY_NAME = "billingCycle";

    private final BillingCycleRepository billingCycleRepository;

	private final DayRepository dayRepository;

	private final BillingCycleService billingCycleService;

	public BillingCycleResource(BillingCycleRepository billingCycleRepository,
			BillingCycleService billingCycleService, DayRepository dayRepository) {
        this.billingCycleRepository = billingCycleRepository;
		this.billingCycleService = billingCycleService;
		this.dayRepository = dayRepository;
    }

    /**
     * POST  /billing-cycles : Create a new billingCycle.
     *
     * @param billingCycle the billingCycle to create
     * @return the ResponseEntity with status 201 (Created) and with body the new billingCycle, or with status 400 (Bad Request) if the billingCycle has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/billing-cycles")
	public ResponseEntity<BillingCycle> createBillingCycle(
			@Valid @RequestBody BillingCycleWithDaysVM billingCycleWithDaysVM) throws URISyntaxException {
		log.debug("REST request to save BillingCycleWithDays : {}", billingCycleWithDaysVM);
		final BillingCycle billingCycle = billingCycleWithDaysVM.getBillingCycle();

		if (billingCycle.getId() != null) {
			throw new BadRequestAlertException("A new billingCycle cannot already have an ID", ENTITY_NAME,
					"idexists");
        }
		
		if (!billingCycleRepository
				.findCyclesOverlappingGivenDates(billingCycle.getStartDate(), billingCycle.getEndDate()).isEmpty()) {
			throw new BadRequestAlertException("A new billing cycle cannot overlap an already existing one",
					ENTITY_NAME, "overlappingdates");
		}

		billingCycleWithDaysVM.getDays().forEach(day -> {
			if (day.getId() != null) {
				throw new BadRequestAlertException("A new day cannot already have an ID", "day", "idexists");
			}
			
			if (day.getBillingCycle() != null) {
				throw new BadRequestAlertException("A new day cannot already have a billing cycle", "day",
						"relationexists");
			}

			validateDay(billingCycle, day);

		});
		
		BillingCycle result = billingCycleService.createBillingCycle(billingCycleWithDaysVM);
        return ResponseEntity.created(new URI("/api/billing-cycles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
	 * PUT /billing-cycles/{id}/close-registration : Close registration.
	 *
	 * @param id id of the billingCycle to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         billingCycle, or with status 400 (Bad Request) if the billingCycle is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         billingCycle couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/billing-cycles/{id}/close-registration")
	public ResponseEntity<BillingCycle> closeRegistration(@PathVariable Long id) throws URISyntaxException {
		log.debug("REST request to close registration for BillingCycle with id : {}", id);
		Optional<BillingCycle> optional = billingCycleRepository.findById(id);

		optional = optional.map(bc -> billingCycleRepository.save(bc.registrationOpen(false)));

		return ResponseUtil.wrapOrNotFound(optional);
    }

    /**
	 * PUT /billing-cycles/days : Update day.
	 *
	 * @param day the day to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         day, or with status 400 (Bad Request) if the day is not valid, or
	 *         with status 500 (Internal Server Error) if the day couldn't be
	 *         updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/billing-cycles/days")
	public ResponseEntity<Day> updateDay(@Valid @RequestBody Day day) throws URISyntaxException {
		log.debug("REST request to update day: {}", day);
		return ResponseUtil.wrapOrNotFound(dayRepository.findById(day.getId())
			.flatMap(d -> d.getDate().equals(day.getDate()) ? Optional.<Day>of(day) : Optional.empty())
				.flatMap(d -> d.getBillingCycle().getId() == day.getBillingCycle().getId() ? Optional.<Day>of(day)
						: Optional.empty())
				.map(d -> validateDay(d.getBillingCycle(), day))
				.map(dayRepository::save));
	}

	/**
	 * GET /billing-cycles : get all the billingCycles.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of billingCycles
	 *         in body
	 */
    @Secured({AuthoritiesConstants.USER})
    @GetMapping("/billing-cycles")
    public List<BillingCycle> getAllBillingCycles() {
        log.debug("REST request to get all BillingCycles");
        return billingCycleRepository.findAll();
    }

    /**
     * GET  /billing-cycles/:id : get the "id" billingCycle.
     *
     * @param id the id of the billingCycle to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the billingCycle, or with status 404 (Not Found)
     */
    @Secured({AuthoritiesConstants.USER})
    @GetMapping("/billing-cycles/{id}")
    public ResponseEntity<BillingCycle> getBillingCycle(@PathVariable Long id) {
        log.debug("REST request to get BillingCycle : {}", id);
        Optional<BillingCycle> billingCycle = billingCycleRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(billingCycle);
    }

    /**
     * GET  /billing-cycles/:id/days : get days of the "id" billingCycle.
     *
     * @param id the id of the billingCycle
     * @return the ResponseEntity with status 200 (OK) and with body the list of days, or with status 404 (Not Found)
     */
    @Secured({AuthoritiesConstants.USER})
    @GetMapping("/billing-cycles/{id}/days")
	public List<Day> getBillingCycleDays(@PathVariable Long id) {
        log.debug("REST request to get BillingCycle days: {}", id);
		return dayRepository.findAllByBillingCycleId(id);
    }

	/**
	 * DELETE /billing-cycles/:id : delete the "id" billingCycle.
	 *
	 * @param id the id of the billingCycle to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
    @DeleteMapping("/billing-cycles/{id}")
    public ResponseEntity<Void> deleteBillingCycle(@PathVariable Long id) {
        log.debug("REST request to delete BillingCycle : {}", id);
		billingCycleService.deleteBillingCycle(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    
	@GetMapping("/billing-cycles/validateDates")
    public Boolean validateDates(@RequestParam("startDate") LocalDate startDate, @RequestParam("endDate") LocalDate endDate) {
		Preconditions.checkArgument(!startDate.isAfter(endDate), "start date cannot be after end date");
		return billingCycleRepository.findCyclesOverlappingGivenDates(startDate, endDate).isEmpty();
	}

	private Day validateDay(final BillingCycle billingCycle, Day day) {
		LocalDate date = day.getDate();
		LocalDate startDate = billingCycle.getStartDate();
		LocalDate endDate = billingCycle.getEndDate();
		if (!((date.isEqual(startDate) || date.isAfter(startDate))
				&& (date.isEqual(endDate) || date.isBefore(endDate)))) {
			throw new BadRequestAlertException(
					String.format("Day (%s) must be within its billing cycle: %s - %s",
							date, startDate, endDate),
					"day",
					"dateoutsidecycle");
		}

		if (day.isOpen()
				&& (day.getStartTimeHour() == null || day.getStartTimeMinute() == null || day.getEndTimeHour() == null)
				|| day.getEndTimeMinute() == null) {
			throw new BadRequestAlertException("When squat is open start time and end time must be defined", "day",
					"timenotdefined");
		}
		return day;
	}

}
