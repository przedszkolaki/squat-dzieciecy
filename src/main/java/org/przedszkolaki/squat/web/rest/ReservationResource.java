package org.przedszkolaki.squat.web.rest;

import static com.google.common.base.Preconditions.checkArgument;
import static org.przedszkolaki.squat.security.SecurityUtils.getCurrentUserLogin;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.przedszkolaki.squat.domain.Reservation;
import org.przedszkolaki.squat.repository.BabyRepository;
import org.przedszkolaki.squat.repository.ReservationRepository;
import org.przedszkolaki.squat.security.AuthoritiesConstants;
import org.przedszkolaki.squat.security.SecurityUtils;
import org.przedszkolaki.squat.web.rest.errors.BadRequestAlertException;
import org.przedszkolaki.squat.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing Reservation.
 */
@Secured({ AuthoritiesConstants.COORDINATOR, AuthoritiesConstants.PARENT })
@RestController
@RequestMapping("/api")
public class ReservationResource {

	private final Logger log = LoggerFactory.getLogger(ReservationResource.class);

	private static final String ENTITY_NAME = "reservation";

	private final ReservationRepository reservationRepository;
	private final BabyRepository babyRepository;

	public ReservationResource(ReservationRepository reservationRepository, BabyRepository babyRepository) {
		this.reservationRepository = reservationRepository;
		this.babyRepository = babyRepository;
	}

	/**
	 * POST /reservations : Create a new reservation.
	 *
	 * @param reservation the reservation to create
	 * @return the ResponseEntity with status 201 (Created) and with body the new
	 *         reservation, or with status 400 (Bad Request) if the reservation has
	 *         already an ID
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PostMapping("/reservations")
	public ResponseEntity<Reservation> createReservation(@Valid @RequestBody Reservation reservation)
			throws URISyntaxException {
		log.debug("REST request to save Reservation : {}", reservation);
		if (reservation.getId() != null) {
			throw new BadRequestAlertException("A new reservation cannot already have an ID", ENTITY_NAME, "idexists");
		}
		babyRepository.findById(reservation.getBaby().getId()).map(baby -> baby.getParent().getUser().getLogin())
				.ifPresent(login -> checkArgument(login.equals(getCurrentUserLogin().get()),
						"user can only add a reservation for his or her baby"));
		Reservation result = reservationRepository.save(reservation);
		return ResponseEntity.created(new URI("/api/reservations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	/**
	 * PUT /reservations : Updates an existing reservation.
	 *
	 * @param reservation the reservation to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         reservation, or with status 400 (Bad Request) if the reservation is
	 *         not valid, or with status 500 (Internal Server Error) if the
	 *         reservation couldn't be updated
	 * @throws URISyntaxException if the Location URI syntax is incorrect
	 */
	@PutMapping("/reservations")
	public ResponseEntity<Reservation> updateReservation(@Valid @RequestBody Reservation reservation)
			throws URISyntaxException {
		log.debug("REST request to update Reservation : {}", reservation);
		if (reservation.getId() == null) {
			throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
		}
		babyRepository.findById(reservation.getBaby().getId()).map(baby -> baby.getParent().getUser().getLogin())
				.ifPresent(login -> checkArgument(login.equals(getCurrentUserLogin().get()),
						"user can only edit a reservation for his or her baby"));
		Reservation result = reservationRepository.save(reservation);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, reservation.getId().toString())).body(result);
	}

	/**
	 * GET /reservations : get all the reservations.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of reservations
	 *         in body
	 */
	@GetMapping("/reservations")
	public List<Reservation> getAllReservations() {
		log.debug("REST request to get all Reservations");
		return reservationRepository.findAll();
	}

	/**
	 * GET /reservations : get upcoming reservations for current user's babies.
	 *
	 * @return the ResponseEntity with status 200 (OK) and the list of reservations
	 *         in body
	 */
	@GetMapping("/reservations/my-upcoming")
	public List<Reservation> getMyUpcomingReservations() {
		log.debug("REST request to get all Reservations");
		return reservationRepository.findByUserLogin(SecurityUtils.getCurrentUserLogin().get());
	}

	/**
	 * GET /reservations/:id : get the "id" reservation.
	 *
	 * @param id the id of the reservation to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         reservation, or with status 404 (Not Found)
	 */
	@GetMapping("/reservations/{id}")
	public ResponseEntity<Reservation> getReservation(@PathVariable Long id) {
		log.debug("REST request to get Reservation : {}", id);
		Optional<Reservation> reservation = reservationRepository.findById(id);
		return ResponseUtil.wrapOrNotFound(reservation);
	}

	/**
	 * DELETE /reservations/:id : delete the "id" reservation.
	 *
	 * @param id the id of the reservation to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/reservations/{id}")
	public ResponseEntity<Void> deleteReservation(@PathVariable Long id) {
		log.debug("REST request to delete Reservation : {}", id);
		reservationRepository.deleteById(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}
}
