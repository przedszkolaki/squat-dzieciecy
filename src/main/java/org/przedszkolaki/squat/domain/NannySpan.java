package org.przedszkolaki.squat.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A NannySpan.
 */
@Entity
@Table(name = "nanny_span")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NannySpan implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Min(value = 8)
    @Max(value = 18)
    @Column(name = "from_hour", nullable = false)
    private Integer fromHour;

    @NotNull
    @Min(value = 8)
    @Max(value = 18)
    @Column(name = "to_hour", nullable = false)
    private Integer toHour;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("nannySpans")
    private Day day;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("nannySpans")
    private Nanny nanny;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFromHour() {
        return fromHour;
    }

    public NannySpan fromHour(Integer fromHour) {
        this.fromHour = fromHour;
        return this;
    }

    public void setFromHour(Integer fromHour) {
        this.fromHour = fromHour;
    }

    public Integer getToHour() {
        return toHour;
    }

    public NannySpan toHour(Integer toHour) {
        this.toHour = toHour;
        return this;
    }

    public void setToHour(Integer toHour) {
        this.toHour = toHour;
    }

    public Day getDay() {
        return day;
    }

    public NannySpan day(Day day) {
        this.day = day;
        return this;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Nanny getNanny() {
        return nanny;
    }

    public NannySpan nanny(Nanny nanny) {
        this.nanny = nanny;
        return this;
    }

    public void setNanny(Nanny nanny) {
        this.nanny = nanny;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NannySpan nannySpan = (NannySpan) o;
        if (nannySpan.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), nannySpan.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NannySpan{" +
            "id=" + getId() +
            ", fromHour=" + getFromHour() +
            ", toHour=" + getToHour() +
            "}";
    }
}
