package org.przedszkolaki.squat.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A Payslip.
 */
@Entity
@Table(name = "payslip")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Payslip implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "amount_to_receive", precision = 10, scale = 2, nullable = false)
    private BigDecimal amountToReceive;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("payslips")
    private BillingCycle billingCycle;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("payslips")
    private Nanny nanny;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmountToReceive() {
        return amountToReceive;
    }

    public Payslip amountToReceive(BigDecimal amountToReceive) {
        this.amountToReceive = amountToReceive;
        return this;
    }

    public void setAmountToReceive(BigDecimal amountToReceive) {
        this.amountToReceive = amountToReceive;
    }

    public BillingCycle getBillingCycle() {
        return billingCycle;
    }

    public Payslip billingCycle(BillingCycle billingCycle) {
        this.billingCycle = billingCycle;
        return this;
    }

    public void setBillingCycle(BillingCycle billingCycle) {
        this.billingCycle = billingCycle;
    }

    public Nanny getNanny() {
        return nanny;
    }

    public Payslip nanny(Nanny nanny) {
        this.nanny = nanny;
        return this;
    }

    public void setNanny(Nanny nanny) {
        this.nanny = nanny;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Payslip payslip = (Payslip) o;
        if (payslip.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), payslip.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Payslip{" +
            "id=" + getId() +
            ", amountToReceive=" + getAmountToReceive() +
            "}";
    }
}
