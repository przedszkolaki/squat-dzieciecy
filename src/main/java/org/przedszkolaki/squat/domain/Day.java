package org.przedszkolaki.squat.domain;


import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Day.
 */
@Entity
@Table(name = "day")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Day implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "jhi_date", nullable = false)
    private LocalDate date;

    @NotNull
    @Column(name = "jhi_open", nullable = false)
    private Boolean open;

    @Max(value = 23)
    @Column(name = "start_time_hour")
    private Integer startTimeHour;

    @Max(value = 59)
    @Column(name = "start_time_minute")
    private Integer startTimeMinute;

    @Max(value = 23)
    @Column(name = "end_time_hour")
    private Integer endTimeHour;

    @Max(value = 59)
    @Column(name = "end_time_minute")
    private Integer endTimeMinute;

    @NotNull
    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "notes")
    private String notes;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("days")
	@JoinColumn(updatable = false)
    private BillingCycle billingCycle;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public Day date(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Boolean isOpen() {
        return open;
    }

    public Day open(Boolean open) {
        this.open = open;
        return this;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public Integer getStartTimeHour() {
        return startTimeHour;
    }

    public Day startTimeHour(Integer startTimeHour) {
        this.startTimeHour = startTimeHour;
        return this;
    }

    public void setStartTimeHour(Integer startTimeHour) {
        this.startTimeHour = startTimeHour;
    }

    public Integer getStartTimeMinute() {
        return startTimeMinute;
    }

    public Day startTimeMinute(Integer startTimeMinute) {
        this.startTimeMinute = startTimeMinute;
        return this;
    }

    public void setStartTimeMinute(Integer startTimeMinute) {
        this.startTimeMinute = startTimeMinute;
    }

    public Integer getEndTimeHour() {
        return endTimeHour;
    }

    public Day endTimeHour(Integer endTimeHour) {
        this.endTimeHour = endTimeHour;
        return this;
    }

    public void setEndTimeHour(Integer endTimeHour) {
        this.endTimeHour = endTimeHour;
    }

    public Integer getEndTimeMinute() {
        return endTimeMinute;
    }

    public Day endTimeMinute(Integer endTimeMinute) {
        this.endTimeMinute = endTimeMinute;
        return this;
    }

    public void setEndTimeMinute(Integer endTimeMinute) {
        this.endTimeMinute = endTimeMinute;
    }

    public String getAddress() {
        return address;
    }

    public Day address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNotes() {
        return notes;
    }

    public Day notes(String notes) {
        this.notes = notes;
        return this;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public BillingCycle getBillingCycle() {
        return billingCycle;
    }

    public Day billingCycle(BillingCycle billingCycle) {
        this.billingCycle = billingCycle;
        return this;
    }

    public void setBillingCycle(BillingCycle billingCycle) {
        this.billingCycle = billingCycle;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Day day = (Day) o;
        if (day.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), day.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Day{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", open='" + isOpen() + "'" +
            ", startTimeHour=" + getStartTimeHour() +
            ", startTimeMinute=" + getStartTimeMinute() +
            ", endTimeHour=" + getEndTimeHour() +
            ", endTimeMinute=" + getEndTimeMinute() +
            ", address='" + getAddress() + "'" +
            ", notes='" + getNotes() + "'" +
            "}";
    }
}
