package org.przedszkolaki.squat.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A ReservationCost.
 */
@Entity
@Table(name = "reservation_cost")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ReservationCost implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Min(value = 8)
    @Max(value = 18)
    @Column(name = "from_hour", nullable = false)
    private Integer fromHour;

    @NotNull
    @Min(value = 8)
    @Max(value = 18)
    @Column(name = "to_hour", nullable = false)
    private Integer toHour;

    @NotNull
    @Column(name = "number_of_babies", nullable = false)
    private Integer numberOfBabies;

    @NotNull
    @Column(name = "number_of_nannies", nullable = false)
    private Integer numberOfNannies;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("reservationCosts")
    private Reservation reservation;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFromHour() {
        return fromHour;
    }

    public ReservationCost fromHour(Integer fromHour) {
        this.fromHour = fromHour;
        return this;
    }

    public void setFromHour(Integer fromHour) {
        this.fromHour = fromHour;
    }

    public Integer getToHour() {
        return toHour;
    }

    public ReservationCost toHour(Integer toHour) {
        this.toHour = toHour;
        return this;
    }

    public void setToHour(Integer toHour) {
        this.toHour = toHour;
    }

    public Integer getNumberOfBabies() {
        return numberOfBabies;
    }

    public ReservationCost numberOfBabies(Integer numberOfBabies) {
        this.numberOfBabies = numberOfBabies;
        return this;
    }

    public void setNumberOfBabies(Integer numberOfBabies) {
        this.numberOfBabies = numberOfBabies;
    }

    public Integer getNumberOfNannies() {
        return numberOfNannies;
    }

    public ReservationCost numberOfNannies(Integer numberOfNannies) {
        this.numberOfNannies = numberOfNannies;
        return this;
    }

    public void setNumberOfNannies(Integer numberOfNannies) {
        this.numberOfNannies = numberOfNannies;
    }

    public Reservation getReservation() {
        return reservation;
    }

    public ReservationCost reservation(Reservation reservation) {
        this.reservation = reservation;
        return this;
    }

    public void setReservation(Reservation reservation) {
        this.reservation = reservation;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReservationCost reservationCost = (ReservationCost) o;
        if (reservationCost.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reservationCost.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ReservationCost{" +
            "id=" + getId() +
            ", fromHour=" + getFromHour() +
            ", toHour=" + getToHour() +
            ", numberOfBabies=" + getNumberOfBabies() +
            ", numberOfNannies=" + getNumberOfNannies() +
            "}";
    }
}
