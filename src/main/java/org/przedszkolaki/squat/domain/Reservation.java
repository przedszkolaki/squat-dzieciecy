package org.przedszkolaki.squat.domain;


import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * A Reservation.
 */
@Entity
@Table(name = "reservation")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Reservation implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	@NotNull
	@Max(value = 23)
	@Column(name = "start_time_hour")
	private Integer startTimeHour;

	@NotNull
	@Max(value = 59)
	@Column(name = "start_time_minute")
	private Integer startTimeMinute;

	@NotNull
	@Max(value = 23)
	@Column(name = "end_time_hour")
	private Integer endTimeHour;

	@NotNull
	@Max(value = 59)
	@Column(name = "end_time_minute")
	private Integer endTimeMinute;

	@ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("reservations")
	@JoinColumn(updatable = false)
    private Baby baby;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("reservations")
	@JoinColumn(updatable = false)
    private Day day;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Baby getBaby() {
        return baby;
    }

    public Reservation baby(Baby baby) {
        this.baby = baby;
        return this;
    }

    public void setBaby(Baby baby) {
        this.baby = baby;
    }

    public Day getDay() {
        return day;
    }

    public Reservation day(Day day) {
        this.day = day;
        return this;
    }

    public void setDay(Day day) {
        this.day = day;
    }

	public Integer getStartTimeHour() {
		return startTimeHour;
	}

	public void setStartTimeHour(Integer startTimeHour) {
		this.startTimeHour = startTimeHour;
	}

	public Reservation startTimeHour(Integer startTimeHour) {
		this.startTimeHour = startTimeHour;
		return this;
	}

	public Integer getStartTimeMinute() {
		return startTimeMinute;
	}

	public void setStartTimeMinute(Integer startTimeMinute) {
		this.startTimeMinute = startTimeMinute;
	}

	public Reservation startTimeMinute(Integer startTimeMinute) {
		this.startTimeMinute = startTimeMinute;
		return this;
	}

	public Integer getEndTimeHour() {
		return endTimeHour;
	}

	public void setEndTimeHour(Integer endTimeHour) {
		this.endTimeHour = endTimeHour;
	}

	public Reservation endTimeHour(Integer endTimeHour) {
		this.endTimeHour = endTimeHour;
		return this;
	}

	public Integer getEndTimeMinute() {
		return endTimeMinute;
	}

	public void setEndTimeMinute(Integer endTimeMinute) {
		this.endTimeMinute = endTimeMinute;
	}

	public Reservation endTimeMinute(Integer endTimeMinute) {
		this.endTimeMinute = endTimeMinute;
		return this;
	}

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Reservation reservation = (Reservation) o;
        if (reservation.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), reservation.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

	@Override
	public String toString() {
		return "Reservation{id=" + id + ", startTimeHour=" + startTimeHour + ", startTimeMinute=" + startTimeMinute
				+ ", endTimeHour=" + endTimeHour + ", endTimeMinute=" + endTimeMinute + "}";
	}

//    @Override
//    public String toString() {
//        return "Reservation{" +
//            "id=" + getId() +
//            ", fromHour=" + getFromHour() +
//            ", toHour=" + getToHour() +
//            "}";
//    }
}
