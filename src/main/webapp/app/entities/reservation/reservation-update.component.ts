import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IReservation } from 'app/shared/model/reservation.model';
import { ReservationService } from './reservation.service';
import { IBaby } from 'app/shared/model/baby.model';
import { BabyService } from 'app/entities/baby';
import { IDay } from 'app/shared/model/day.model';

@Component({
    selector: 'jhi-reservation-update',
    templateUrl: './reservation-update.component.html'
})
export class ReservationUpdateComponent implements OnInit {
    reservation: IReservation;
    isSaving: boolean;

    babies: IBaby[];

    days: IDay[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected reservationService: ReservationService,
        protected babyService: BabyService,
        //        protected dayService: DayService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ reservation }) => {
            this.reservation = reservation;
        });
        this.babyService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IBaby[]>) => mayBeOk.ok),
                map((response: HttpResponse<IBaby[]>) => response.body)
            )
            .subscribe((res: IBaby[]) => (this.babies = res), (res: HttpErrorResponse) => this.onError(res.message));
        // this.dayService
        //     .query()
        //     .pipe(
        //         filter((mayBeOk: HttpResponse<IDay[]>) => mayBeOk.ok),
        //         map((response: HttpResponse<IDay[]>) => response.body)
        //     )
        //     .subscribe((res: IDay[]) => (this.days = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.reservation.id !== undefined) {
            this.subscribeToSaveResponse(this.reservationService.update(this.reservation));
        } else {
            this.subscribeToSaveResponse(this.reservationService.create(this.reservation));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IReservation>>) {
        result.subscribe((res: HttpResponse<IReservation>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackBabyById(index: number, item: IBaby) {
        return item.id;
    }

    trackDayById(index: number, item: IDay) {
        return item.id;
    }
}
