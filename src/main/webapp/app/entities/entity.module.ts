import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'baby',
                loadChildren: './baby/baby.module#SquatBabyModule'
            },
            {
                path: 'billing-cycle',
                loadChildren: './billing-cycle/billing-cycle.module#SquatBillingCycleModule'
            },
            {
                path: 'parent',
                loadChildren: './parent/parent.module#SquatParentModule'
            },
            {
                path: 'reservation',
                loadChildren: './reservation/reservation.module#SquatReservationModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SquatEntityModule {}
