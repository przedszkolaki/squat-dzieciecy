import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { BillingCycle } from 'app/shared/model/billing-cycle.model';
import { BillingCycleService } from './billing-cycle.service';
import { BillingCycleComponent } from './billing-cycle.component';
import { BillingCycleUpdateComponent } from './billing-cycle-update.component';
import { BillingCycleDeletePopupComponent } from './billing-cycle-delete-dialog.component';
import { IBillingCycle } from 'app/shared/model/billing-cycle.model';

@Injectable({ providedIn: 'root' })
export class BillingCycleResolve implements Resolve<IBillingCycle> {
    constructor(private service: BillingCycleService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBillingCycle> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<BillingCycle>) => response.ok),
                map((billingCycle: HttpResponse<BillingCycle>) => billingCycle.body)
            );
        }
        return of(new BillingCycle());
    }
}

export const billingCycleRoute: Routes = [
    {
        path: '',
        component: BillingCycleComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BillingCycles'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: BillingCycleUpdateComponent,
        resolve: {
            billingCycle: BillingCycleResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BillingCycles'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: BillingCycleUpdateComponent,
        resolve: {
            billingCycle: BillingCycleResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BillingCycles'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const billingCyclePopupRoute: Routes = [
    {
        path: ':id/delete',
        component: BillingCycleDeletePopupComponent,
        resolve: {
            billingCycle: BillingCycleResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'BillingCycles'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
