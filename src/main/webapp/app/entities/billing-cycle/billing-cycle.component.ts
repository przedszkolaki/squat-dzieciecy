import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IBillingCycle } from 'app/shared/model/billing-cycle.model';
import { AccountService } from 'app/core';
import { BillingCycleService } from './billing-cycle.service';

@Component({
    selector: 'jhi-billing-cycle',
    templateUrl: './billing-cycle.component.html'
})
export class BillingCycleComponent implements OnInit, OnDestroy {
    billingCycles: IBillingCycle[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected billingCycleService: BillingCycleService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.billingCycleService
            .query()
            .pipe(
                filter((res: HttpResponse<IBillingCycle[]>) => res.ok),
                map((res: HttpResponse<IBillingCycle[]>) => res.body)
            )
            .subscribe(
                (res: IBillingCycle[]) => {
                    this.billingCycles = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInBillingCycles();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IBillingCycle) {
        return item.id;
    }

    registerChangeInBillingCycles() {
        this.eventSubscriber = this.eventManager.subscribe('billingCycleListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
