import { Component, Input } from '@angular/core';
import { IDay } from '../../shared/model/day.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DayEditDialogContentComponent } from './day-edit-dialog.component';
import { BillingCycleService } from './billing-cycle.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'jhi-day-vm',
    template: `
        <div (click)="open(day)">
            <div>
                {{ day.date.format('D') }}
            </div>
            <div>od: {{ day.startTime.hour }}.{{ day.startTime.minute | number: '2.0-0' }}</div>
            <div>do: {{ day.endTime.hour }}.{{ day.endTime.minute | number: '2.0-0' }}</div>
            <div>{{ day.address || ' ' }}</div>
            <div>{{ day.notes || ' ' }}</div>
        </div>
    `
})
export class DayComponent {
    @Input() day: IDay;

    constructor(private modalService: NgbModal, private billingCycleService: BillingCycleService) {}

    open(day: IDay) {
        const modalRef = this.modalService.open(DayEditDialogContentComponent);
        modalRef.componentInstance.dayVM = day;
        if (day.id != null) {
            modalRef.result.then(res => {
                this.billingCycleService.updateDay(day).subscribe((httpRes: HttpResponse<IDay>) => {});
            });
        }
    }
}
