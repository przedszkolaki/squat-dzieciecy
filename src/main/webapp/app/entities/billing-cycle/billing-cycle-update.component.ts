import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { IBillingCycle } from 'app/shared/model/billing-cycle.model';
import { BillingCycleService } from './billing-cycle.service';
import { JhiAlertService } from 'ng-jhipster';
import { Day } from 'app/shared/model/day.model';
import { Moment } from 'moment';

@Component({
    selector: 'jhi-billing-cycle-update',
    templateUrl: './billing-cycle-update.component.html'
})
export class BillingCycleUpdateComponent implements OnInit {
    billingCycle: IBillingCycle;
    isSaving: boolean;
    isValidatingDates: boolean;
    isClosingRegistration: boolean;
    secondStep: boolean;
    startDateDp: any;
    endDateDp: any;
    tableValidationErrors: Array<string> = [];
    private days: Day[];
    daysObs: Subject<Day[]> = new BehaviorSubject([]);

    constructor(
        protected billingCycleService: BillingCycleService,
        protected jhiAlertService: JhiAlertService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ billingCycle }) => {
            this.billingCycle = billingCycle;
            if (this.billingCycle.id != null) {
                this.secondStep = true;
                this.billingCycleService.findAllDaysByBillingCycleId(this.billingCycle.id).subscribe(res => {
                    this.days = res.body;
                    this.daysObs.next(this.days);
                });
            }
        });
    }

    previousState() {
        window.history.back();
    }

    closeRegistration() {
        this.isClosingRegistration = true;
        this.billingCycleService.closeRegistration(this.billingCycle.id).subscribe(
            (res: HttpResponse<IBillingCycle>) => {
                this.billingCycle.registrationOpen = false;
                this.isClosingRegistration = false;
            },
            (res: HttpErrorResponse) => {
                this.isClosingRegistration = false;
            }
        );
    }

    save() {
        this.isSaving = true;
        if (!this.validateTable()) {
            return;
        }
        if (this.billingCycle.id !== undefined) {
            this.subscribeToSaveResponse(this.billingCycleService.update(this.billingCycle));
        } else {
            this.subscribeToSaveResponse(this.billingCycleService.create(this.billingCycle, this.days));
        }
    }

    goToSecondStep() {
        this.isValidatingDates = true;
        this.validateDates().subscribe((res: HttpResponse<boolean>) => {
            if (res) {
                this.secondStep = true;
                this.generateDays();
                this.daysObs.next(this.days);
            } else {
                this.jhiAlertService.error('Okresy rozliczeniowe muszą być rozłączne');
            }
            this.isValidatingDates = false;
        });
    }

    goToFirstStep() {
        this.secondStep = false;
    }

    validateDates(): Observable<HttpResponse<boolean>> {
        return this.billingCycleService.validateDates(this.billingCycle.startDate, this.billingCycle.endDate);
    }

    generateDays() {
        const day: Moment = this.billingCycle.startDate.clone();
        this.days = [];
        while (day.isSameOrBefore(this.billingCycle.endDate)) {
            this.days.push(new Day(day.clone()));
            day.add(1, 'd');
        }
    }

    private validateTable(): boolean {
        this.days.forEach(day => {
            //                this.tableValidationErrors.push('ahha nana.');
        });
        return this.tableValidationErrors.length === 0;
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IBillingCycle>>) {
        result.subscribe((res: HttpResponse<IBillingCycle>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }
}
