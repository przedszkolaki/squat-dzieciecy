import { Component, Input, OnInit } from '@angular/core';
import { IDay, Day } from 'app/shared/model/day.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DayEditDialogContentComponent } from './day-edit-dialog.component';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { BillingCycleService } from './billing-cycle.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { JhiAlertService } from 'ng-jhipster';
import { unix } from 'moment';
import { cloneTime } from 'app/shared/model/time.model';

@Component({
    selector: 'jhi-billing-cycle-callendar',
    templateUrl: './billing-cycle-callendar.component.html'
})
export class BillingCycleCallendarComponent implements OnInit {
    weeks: Array<Array<IDay>> = [];
    filling: Array<any> = [];
    @Input() daysObs: Observable<Day[]>;

    constructor(private modalService: NgbModal, private billingCycleService: BillingCycleService, private alertService: JhiAlertService) {}

    ngOnInit() {
        this.daysObs.pipe(filter(days => days.length > 0)).subscribe((days: Day[]) => {
            const startDay = days[0].date;
            const filling = startDay.diff(startDay.clone().startOf('isoWeek'), 'days');
            for (let i = 0; i < filling; i++) {
                this.filling.push(null);
            }

            let week: Array<IDay> = [];
            for (const day of days) {
                week.push(day);
                if (day.date.isoWeekday() === 7) {
                    this.weeks.push(week);
                    week = [];
                }
            }
            if (week.length !== 0) {
                this.weeks.push(week);
            }
        });
    }

    openSetWeekdayDialog(n: number, title: string) {
        const resultDay = new Day(unix(0));
        const modalRef = this.modalService.open(DayEditDialogContentComponent);
        modalRef.componentInstance.title = title;
        modalRef.componentInstance.dayVM = resultDay;
        modalRef.result.then(res => {
            this.weeks.forEach(days =>
                days.forEach(d => {
                    if (d.date.isoWeekday() === n) {
                        d.address = resultDay.address.slice(0);
                        d.notes = (resultDay.notes || '').slice(0);
                        d.startTime = cloneTime(resultDay.startTime);
                        d.endTime = cloneTime(resultDay.endTime);
                        d.open = resultDay.open;
                        if (d.id != null) {
                            this.billingCycleService.updateDay(d).subscribe(
                                (httpRes: HttpResponse<IDay>) => {
                                    this.alertService.error(httpRes.body.date.day.name);
                                },
                                (httpRes: HttpErrorResponse) => {
                                    this.alertService.error(httpRes.error);
                                }
                            );
                        }
                    }
                })
            );
        });
    }
}
