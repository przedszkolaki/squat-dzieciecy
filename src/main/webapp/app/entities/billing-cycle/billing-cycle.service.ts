import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBillingCycle, IBillingCycleWithDays, BillingCycle } from 'app/shared/model/billing-cycle.model';
import { Moment } from 'moment';
import { Day } from 'app/shared/model/day.model';
import { IDay } from '../../shared/model/day.model';
import { convertTimeFromClient, convertTimeFromServer } from 'app/shared/model/time.model';

type EntityResponseType = HttpResponse<IBillingCycle>;
type EntityArrayResponseType = HttpResponse<IBillingCycle[]>;

@Injectable({ providedIn: 'root' })
export class BillingCycleService {
    public resourceUrl = SERVER_API_URL + 'api/billing-cycles';

    constructor(protected http: HttpClient) {}

    create(billingCycle: IBillingCycle, days: Array<IDay>): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(billingCycle);
        const daysCopy = days.map(day => this.convertDayFromClient(day));
        const billingCycleWithDays = { billingCycle: copy, days: daysCopy };
        return this.http
            .post<IBillingCycle>(this.resourceUrl, billingCycleWithDays, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    update(billingCycle: IBillingCycle): Observable<EntityResponseType> {
        const copy = this.convertDateFromClient(billingCycle);
        return this.http
            .put<IBillingCycle>(this.resourceUrl, copy, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    updateDay(day: IDay): Observable<HttpResponse<IDay>> {
        const copy = this.convertDayFromClient(day);
        return this.http.put<any>(`${this.resourceUrl}/days`, copy, { observe: 'response' }).pipe(
            map((res: HttpResponse<any>) => {
                if (res.body) {
                    this.convertDayFromServer(res.body);
                }
                return res;
            })
        );
    }

    closeRegistration(id: number): Observable<HttpResponse<IBillingCycle>> {
        return this.http
            .put<BillingCycle>(`${this.resourceUrl}/${id}/close-registration`, null, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http
            .get<IBillingCycle>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http
            .get<IBillingCycle[]>(this.resourceUrl, { params: options, observe: 'response' })
            .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    validateDates(startDate: Moment, endDate: Moment): Observable<HttpResponse<boolean>> {
        return this.http.get<boolean>(
            `${this.resourceUrl}/validateDates?startDate=${startDate.format(DATE_FORMAT)}&endDate=${endDate.format(DATE_FORMAT)}`,
            { observe: 'response' }
        );
    }

    findAllDaysByBillingCycleId(id: number): Observable<HttpResponse<Day[]>> {
        return this.http
            .get<Day[]>(`${this.resourceUrl}/${id}/days`, { observe: 'response' })
            .pipe(map((res: HttpResponse<any[]>) => this.convertDaysFromServer(res)));
    }

    protected convertDateFromClient(billingCycle: IBillingCycle): IBillingCycle {
        const copy: IBillingCycle = Object.assign({}, billingCycle, {
            startDate:
                billingCycle.startDate != null && billingCycle.startDate.isValid() ? billingCycle.startDate.format(DATE_FORMAT) : null,
            endDate: billingCycle.endDate != null && billingCycle.endDate.isValid() ? billingCycle.endDate.format(DATE_FORMAT) : null
        });
        return copy;
    }

    protected convertDayFromClient(day: IDay) {
        let copy: IDay = Object.assign({}, day, {
            date: day.date != null && day.date.isValid() ? day.date.format(DATE_FORMAT) : null
        });
        copy = convertTimeFromClient(copy, 'start');
        copy = convertTimeFromClient(copy, 'end');
        return copy;
    }

    protected convertDayFromServer(day: any) {
        Object.assign(day, {
            date: day.date != null ? moment(day.date) : null
        });
        convertTimeFromServer(day, 'start');
        convertTimeFromServer(day, 'end');
    }

    protected convertDaysFromServer(res: HttpResponse<any[]>): HttpResponse<Day[]> {
        if (res.body) {
            res.body.forEach(this.convertDayFromServer);
        }
        return res;
    }

    protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
        if (res.body) {
            res.body.startDate = res.body.startDate != null ? moment(res.body.startDate) : null;
            res.body.endDate = res.body.endDate != null ? moment(res.body.endDate) : null;
        }
        return res;
    }

    protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
        if (res.body) {
            res.body.forEach((billingCycle: IBillingCycle) => {
                billingCycle.startDate = billingCycle.startDate != null ? moment(billingCycle.startDate) : null;
                billingCycle.endDate = billingCycle.endDate != null ? moment(billingCycle.endDate) : null;
            });
        }
        return res;
    }
}
