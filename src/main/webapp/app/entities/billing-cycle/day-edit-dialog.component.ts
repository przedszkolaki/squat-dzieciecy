import { Component, Input } from '@angular/core';
import { IDay, cloneDay } from '../../shared/model/day.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'jhi-day-vm-edit-dialog-content',
    templateUrl: 'day-edit-dialog.component.html'
})
export class DayEditDialogContentComponent {
    private dayVMOrig: IDay;
    private dayVMCopy: IDay;
    private _title: string;

    @Input()
    get title() {
        return this._title || this.datePipe.transform(this.dayVM.date, 'fullDate');
    }
    set title(title: string) {
        this._title = title;
    }

    @Input()
    get dayVM() {
        return this.dayVMCopy;
    }
    set dayVM(dayVM: IDay) {
        this.dayVMOrig = dayVM;
        this.dayVMCopy = cloneDay(dayVM);
    }

    constructor(public activeModal: NgbActiveModal, private datePipe: DatePipe) {}

    save() {
        this.dayVMOrig.address = this.dayVMCopy.address;
        this.dayVMOrig.open = this.dayVMCopy.open;
        this.dayVMOrig.notes = this.dayVMCopy.notes;
        this.dayVMOrig.startTime = this.dayVMCopy.startTime;
        this.dayVMOrig.endTime = this.dayVMCopy.endTime;
        this.activeModal.close('save');
    }
}
