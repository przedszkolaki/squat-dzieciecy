import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBillingCycle } from 'app/shared/model/billing-cycle.model';
import { BillingCycleService } from './billing-cycle.service';

@Component({
    selector: 'jhi-billing-cycle-delete-dialog',
    templateUrl: './billing-cycle-delete-dialog.component.html'
})
export class BillingCycleDeleteDialogComponent {
    billingCycle: IBillingCycle;

    constructor(
        protected billingCycleService: BillingCycleService,
        public activeModal: NgbActiveModal,
        protected eventManager: JhiEventManager
    ) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.billingCycleService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'billingCycleListModification',
                content: 'Deleted an billingCycle'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-billing-cycle-delete-popup',
    template: ''
})
export class BillingCycleDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ billingCycle }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(BillingCycleDeleteDialogComponent as Component, {
                    size: 'lg',
                    backdrop: 'static'
                });
                this.ngbModalRef.componentInstance.billingCycle = billingCycle;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/billing-cycle', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/billing-cycle', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
