import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SquatSharedModule } from 'app/shared';
import {
    BillingCycleComponent,
    BillingCycleUpdateComponent,
    BillingCycleDeletePopupComponent,
    BillingCycleDeleteDialogComponent,
    billingCycleRoute,
    billingCyclePopupRoute
} from './';
import { DayComponent } from './day.component';
import { DayEditDialogContentComponent } from './day-edit-dialog.component';
import { BillingCycleCallendarComponent } from './billing-cycle-callendar.component';

const ENTITY_STATES = [...billingCycleRoute, ...billingCyclePopupRoute];

@NgModule({
    imports: [SquatSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        BillingCycleComponent,
        BillingCycleUpdateComponent,
        BillingCycleDeleteDialogComponent,
        BillingCycleDeletePopupComponent,
        DayComponent,
        DayEditDialogContentComponent,
        BillingCycleCallendarComponent
    ],
    entryComponents: [
        BillingCycleComponent,
        BillingCycleUpdateComponent,
        BillingCycleDeleteDialogComponent,
        BillingCycleDeletePopupComponent,
        DayComponent,
        DayEditDialogContentComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SquatBillingCycleModule {}
