import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBaby } from 'app/shared/model/baby.model';

@Component({
    selector: 'jhi-baby-detail',
    templateUrl: './baby-detail.component.html'
})
export class BabyDetailComponent implements OnInit {
    baby: IBaby;

    constructor(protected activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ baby }) => {
            this.baby = baby;
        });
    }

    previousState() {
        window.history.back();
    }
}
