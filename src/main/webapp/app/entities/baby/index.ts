export * from './baby.service';
export * from './baby-update.component';
export * from './baby-delete-dialog.component';
export * from './baby-detail.component';
export * from './baby.component';
export * from './baby.route';
