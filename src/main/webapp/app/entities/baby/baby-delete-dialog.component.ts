import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBaby } from 'app/shared/model/baby.model';
import { BabyService } from './baby.service';

@Component({
    selector: 'jhi-baby-delete-dialog',
    templateUrl: './baby-delete-dialog.component.html'
})
export class BabyDeleteDialogComponent {
    baby: IBaby;

    constructor(protected babyService: BabyService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.babyService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'babyListModification',
                content: 'Deleted an baby'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-baby-delete-popup',
    template: ''
})
export class BabyDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ baby }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(BabyDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.baby = baby;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/baby', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/baby', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
