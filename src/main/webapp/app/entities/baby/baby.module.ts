import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SquatSharedModule } from 'app/shared';
import {
    BabyComponent,
    BabyDetailComponent,
    BabyUpdateComponent,
    BabyDeletePopupComponent,
    BabyDeleteDialogComponent,
    babyRoute,
    babyPopupRoute
} from './';

const ENTITY_STATES = [...babyRoute, ...babyPopupRoute];

@NgModule({
    imports: [SquatSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [BabyComponent, BabyDetailComponent, BabyUpdateComponent, BabyDeleteDialogComponent, BabyDeletePopupComponent],
    entryComponents: [BabyComponent, BabyUpdateComponent, BabyDeleteDialogComponent, BabyDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SquatBabyModule {}
