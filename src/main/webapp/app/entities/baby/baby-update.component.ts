import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IBaby } from 'app/shared/model/baby.model';
import { BabyService } from './baby.service';
import { IParent } from 'app/shared/model/parent.model';
import { ParentService } from 'app/entities/parent';

@Component({
    selector: 'jhi-baby-update',
    templateUrl: './baby-update.component.html'
})
export class BabyUpdateComponent implements OnInit {
    baby: IBaby;
    isSaving: boolean;

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected babyService: BabyService,
        protected parentService: ParentService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ baby }) => {
            this.baby = baby;
            this.parentService
                .findForCurrentUser()
                .pipe(
                    filter((mayBeOk: HttpResponse<IParent>) => mayBeOk.ok),
                    map((response: HttpResponse<IParent>) => response.body)
                )
                .subscribe((res: IParent) => (this.baby.parent = res), (res: HttpErrorResponse) => this.onError(res.message));
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.baby.id !== undefined) {
            this.subscribeToSaveResponse(this.babyService.update(this.baby));
        } else {
            this.subscribeToSaveResponse(this.babyService.create(this.baby));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IBaby>>) {
        result.subscribe((res: HttpResponse<IBaby>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackParentById(index: number, item: IParent) {
        return item.id;
    }
}
