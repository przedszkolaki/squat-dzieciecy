import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IBaby } from 'app/shared/model/baby.model';

type EntityResponseType = HttpResponse<IBaby>;
type EntityArrayResponseType = HttpResponse<IBaby[]>;

@Injectable({ providedIn: 'root' })
export class BabyService {
    public resourceUrl = SERVER_API_URL + 'api/babies';

    constructor(protected http: HttpClient) {}

    create(baby: IBaby): Observable<EntityResponseType> {
        return this.http.post<IBaby>(this.resourceUrl, baby, { observe: 'response' });
    }

    update(baby: IBaby): Observable<EntityResponseType> {
        return this.http.put<IBaby>(this.resourceUrl, baby, { observe: 'response' });
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<IBaby>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    query(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IBaby[]>(this.resourceUrl, { params: options, observe: 'response' });
    }

    queryForCurrentUser(req?: any): Observable<EntityArrayResponseType> {
        const options = createRequestOption(req);
        return this.http.get<IBaby[]>(`${this.resourceUrl}/forCurrentUser`, { params: options, observe: 'response' });
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
}
