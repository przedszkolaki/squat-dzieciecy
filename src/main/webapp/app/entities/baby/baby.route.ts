import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Baby } from 'app/shared/model/baby.model';
import { BabyService } from './baby.service';
import { BabyComponent } from './baby.component';
import { BabyDetailComponent } from './baby-detail.component';
import { BabyUpdateComponent } from './baby-update.component';
import { BabyDeletePopupComponent } from './baby-delete-dialog.component';
import { IBaby } from 'app/shared/model/baby.model';

@Injectable({ providedIn: 'root' })
export class BabyResolve implements Resolve<IBaby> {
    constructor(private service: BabyService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IBaby> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Baby>) => response.ok),
                map((baby: HttpResponse<Baby>) => baby.body)
            );
        }
        return of(new Baby());
    }
}

export const babyRoute: Routes = [
    {
        path: '',
        component: BabyComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Babies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: BabyDetailComponent,
        resolve: {
            baby: BabyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Babies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: BabyUpdateComponent,
        resolve: {
            baby: BabyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Babies'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: BabyUpdateComponent,
        resolve: {
            baby: BabyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Babies'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const babyPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: BabyDeletePopupComponent,
        resolve: {
            baby: BabyResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Babies'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
