import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IBaby } from 'app/shared/model/baby.model';
import { AccountService } from 'app/core';
import { BabyService } from './baby.service';

@Component({
    selector: 'jhi-baby',
    templateUrl: './baby.component.html'
})
export class BabyComponent implements OnInit, OnDestroy {
    babies: IBaby[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected babyService: BabyService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.babyService
            .queryForCurrentUser()
            .pipe(
                filter((res: HttpResponse<IBaby[]>) => res.ok),
                map((res: HttpResponse<IBaby[]>) => res.body)
            )
            .subscribe(
                (res: IBaby[]) => {
                    this.babies = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInBabies();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IBaby) {
        return item.id;
    }

    registerChangeInBabies() {
        this.eventSubscriber = this.eventManager.subscribe('babyListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
