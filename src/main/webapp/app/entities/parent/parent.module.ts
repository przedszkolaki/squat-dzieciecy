import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SquatSharedModule } from 'app/shared';
import {
    ParentComponent,
    ParentUpdateComponent,
    ParentDeletePopupComponent,
    ParentDeleteDialogComponent,
    parentRoute,
    parentPopupRoute
} from './';

const ENTITY_STATES = [...parentRoute, ...parentPopupRoute];

@NgModule({
    imports: [SquatSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [ParentComponent, ParentUpdateComponent, ParentDeleteDialogComponent, ParentDeletePopupComponent],
    entryComponents: [ParentComponent, ParentUpdateComponent, ParentDeleteDialogComponent, ParentDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SquatParentModule {}
