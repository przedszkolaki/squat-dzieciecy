export * from './parent.service';
export * from './parent-update.component';
export * from './parent-delete-dialog.component';
export * from './parent.component';
export * from './parent.route';
