import { Component } from '@angular/core';
import { VERSION, BUILD_TIMESTAMP } from 'app/app.constants';

@Component({
    selector: 'jhi-footer',
    templateUrl: './footer.component.html'
})
export class FooterComponent {
    version = VERSION && BUILD_TIMESTAMP ? `v${VERSION}` : '';
    buildTimestamp = new Date(+BUILD_TIMESTAMP);
}
