import { NgModule } from '@angular/core';

import { SquatSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [SquatSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [SquatSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class SquatSharedCommonModule {}
