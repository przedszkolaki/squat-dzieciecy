import { IBaby } from 'app/shared/model/baby.model';
import { IDay } from 'app/shared/model/day.model';

export interface IReservation {
    id?: number;
    fromHour?: number;
    toHour?: number;
    baby?: IBaby;
    day?: IDay;
}

export class Reservation implements IReservation {
    constructor(public id?: number, public fromHour?: number, public toHour?: number, public baby?: IBaby, public day?: IDay) {}
}
