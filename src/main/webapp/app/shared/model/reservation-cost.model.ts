import { IReservation } from 'app/shared/model/reservation.model';

export interface IReservationCost {
    id?: number;
    fromHour?: number;
    toHour?: number;
    numberOfBabies?: number;
    numberOfNannies?: number;
    reservation?: IReservation;
}

export class ReservationCost implements IReservationCost {
    constructor(
        public id?: number,
        public fromHour?: number,
        public toHour?: number,
        public numberOfBabies?: number,
        public numberOfNannies?: number,
        public reservation?: IReservation
    ) {}
}
