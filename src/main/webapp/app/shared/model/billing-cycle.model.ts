import { Moment } from 'moment';
import { IDay } from './day.model';

export interface IBillingCycle {
    id?: number;
    startDate?: Moment;
    endDate?: Moment;
    registrationOpen: Boolean;
}

export class BillingCycle implements IBillingCycle {
    constructor(public id?: number, public startDate?: Moment, public endDate?: Moment, public registrationOpen = true) {}
}

export interface IBillingCycleWithDays {
    billingCycle: IBillingCycle;
    days: Array<IDay>;
}
