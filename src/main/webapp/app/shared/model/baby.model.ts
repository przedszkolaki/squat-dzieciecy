import { IParent } from 'app/shared/model/parent.model';

export interface IBaby {
    id?: number;
    name?: string;
    notes?: string;
    parent?: IParent;
}

export class Baby implements IBaby {
    constructor(public id?: number, public name?: string, public notes?: string, public parent?: IParent) {}
}
