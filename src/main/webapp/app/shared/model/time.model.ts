export interface ITime {
    hour: number;
    minute: number;
}

export class Time implements ITime {
    constructor(public hour = 0, public minute = 0) {}
}

export function cloneTime(time?: ITime): ITime {
    return time === null ? new Time() : new Time(time.hour, time.minute);
}

export function convertTimeFromServer(obj: any, prefix: string): any {
    obj[`${prefix}Time`] = { hour: obj[`${prefix}TimeHour`], minute: obj[`${prefix}TimeMinute`] };
    delete obj[`${prefix}TimeHour`];
    delete obj[`${prefix}TimeMinute`];
    return obj;
}

export function convertTimeFromClient(obj: any, prefix: string): any {
    const copy = Object.assign({}, obj);
    copy[`${prefix}TimeHour`] = obj[`${prefix}Time`].hour;
    copy[`${prefix}TimeMinute`] = obj[`${prefix}Time`].minute;
    return copy;
}
