import { IParent } from 'app/shared/model/parent.model';
import { IBillingCycle } from 'app/shared/model/billing-cycle.model';

export interface IInvoice {
    id?: number;
    amountToPay?: number;
    parent?: IParent;
    billingCycle?: IBillingCycle;
}

export class Invoice implements IInvoice {
    constructor(public id?: number, public amountToPay?: number, public parent?: IParent, public billingCycle?: IBillingCycle) {}
}
