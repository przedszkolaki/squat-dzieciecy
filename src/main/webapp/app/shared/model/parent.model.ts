import { IUser } from 'app/core/user/user.model';

export interface IParent {
    id?: number;
    user?: IUser;
}

export class Parent implements IParent {
    constructor(public id?: number, public user?: IUser) {}
}
