import { IUser } from 'app/core/user/user.model';

export interface INanny {
    id?: number;
    user?: IUser;
}

export class Nanny implements INanny {
    constructor(public id?: number, public user?: IUser) {}
}
