import { IBillingCycle } from 'app/shared/model/billing-cycle.model';
import { INanny } from 'app/shared/model/nanny.model';

export interface IPayslip {
    id?: number;
    amountToReceive?: number;
    billingCycle?: IBillingCycle;
    nanny?: INanny;
}

export class Payslip implements IPayslip {
    constructor(public id?: number, public amountToReceive?: number, public billingCycle?: IBillingCycle, public nanny?: INanny) {}
}
