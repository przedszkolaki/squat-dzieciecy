import { Moment } from 'moment';
import { ITime, cloneTime, Time } from './time.model';

export interface IDay {
    id?: number;
    date: Moment;
    startTime?: ITime;
    endTime?: ITime;
    address?: string;
    notes?: string;
    open?: boolean;
}

export function cloneDay(dayVM: IDay): Day {
    return new Day(
        dayVM.date.clone(),
        dayVM.id,
        cloneTime(dayVM.startTime),
        cloneTime(dayVM.endTime),
        (dayVM.address || '').slice(0),
        (dayVM.notes || '').slice(0),
        dayVM.open
    );
}

export class Day implements IDay {
    constructor(
        public date: Moment,
        public id?: number,
        public startTime: ITime = new Time(),
        public endTime: ITime = new Time(),
        public address = '',
        public notes = '',
        public open = false
    ) {}
}
