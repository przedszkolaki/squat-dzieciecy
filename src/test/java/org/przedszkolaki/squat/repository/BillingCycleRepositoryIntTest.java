package org.przedszkolaki.squat.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.przedszkolaki.squat.SquatApp;
import org.przedszkolaki.squat.domain.BillingCycle;
import org.przedszkolaki.squat.repository.BillingCycleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test class for the BillingCycleResource REST controller.
 *
 * @see BillingCycleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SquatApp.class)
public class BillingCycleRepositoryIntTest {

	private static final LocalDate END_DATE = LocalDate.of(2119, 1, 20);
	private static final LocalDate START_DATE = LocalDate.of(2119, 1, 10);
	@Autowired
    private BillingCycleRepository billingCycleRepository;
	private BillingCycle billingCycle;

	@Before
	public void setup() {
		billingCycle = billingCycleRepository.save(new BillingCycle().startDate(START_DATE).endDate(END_DATE));
	}

	@After
	public void cleanup() {
		billingCycleRepository.delete(billingCycle);
	}

	@Test
	@Transactional
	public void shouldAcceptValidDates() throws Exception {
		Integer[][] cases = new Integer[][] { { 1, 2 }, { 1, 9 }, { 21, 22 }, { 21, 31 } };

		for (Integer[] testCase : cases) {
			// when
			LocalDate startDate = LocalDate.of(2119, 1, testCase[0]);
			LocalDate endDate = LocalDate.of(2119, 1, testCase[1]);
			List<BillingCycle> cycles = billingCycleRepository.findCyclesOverlappingGivenDates(startDate, endDate);

			// then
			assertThat(cycles).withFailMessage("fails for start date: %s, end date: %s", startDate, endDate).isEmpty();
		}
	}

	@Test
	@Transactional
	public void shouldRejectInvalidDates() throws Exception {
		// given
		Integer[][] cases = new Integer[][] { { 1, 10 }, { 1, 11 }, { 10, 20 }, { 10, 11 }, { 11, 20 }, { 20, 21 },
				{ 1, 30 } };

		for (Integer[] testCase : cases) {
			// when
			LocalDate startDate = LocalDate.of(2119, 1, testCase[0]);
			LocalDate endDate = LocalDate.of(2119, 1, testCase[1]);
			List<BillingCycle> cycles = billingCycleRepository.findCyclesOverlappingGivenDates(startDate, endDate);

			// then
			assertThat(cycles).withFailMessage("fails for start date: %s, end date: %s", startDate, endDate)
					.isNotEmpty();
		}
	}

}
