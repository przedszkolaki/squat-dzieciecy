package org.przedszkolaki.squat.web.rest;

import org.przedszkolaki.squat.SquatApp;

import org.przedszkolaki.squat.domain.Nanny;
import org.przedszkolaki.squat.domain.User;
import org.przedszkolaki.squat.repository.NannyRepository;
import org.przedszkolaki.squat.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static org.przedszkolaki.squat.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NannyResource REST controller.
 *
 * @see NannyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SquatApp.class)
public class NannyResourceIntTest {

    @Autowired
    private NannyRepository nannyRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNannyMockMvc;

    private Nanny nanny;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NannyResource nannyResource = new NannyResource(nannyRepository);
        this.restNannyMockMvc = MockMvcBuilders.standaloneSetup(nannyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Nanny createEntity(EntityManager em) {
        Nanny nanny = new Nanny();
        // Add required entity
        User user = UserResourceIntTest.createEntity(em);
        em.persist(user);
        em.flush();
        nanny.setUser(user);
        return nanny;
    }

    @Before
    public void initTest() {
        nanny = createEntity(em);
    }

    @Test
    @Transactional
    public void createNanny() throws Exception {
        int databaseSizeBeforeCreate = nannyRepository.findAll().size();

        // Create the Nanny
        restNannyMockMvc.perform(post("/api/nannies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nanny)))
            .andExpect(status().isCreated());

        // Validate the Nanny in the database
        List<Nanny> nannyList = nannyRepository.findAll();
        assertThat(nannyList).hasSize(databaseSizeBeforeCreate + 1);
        Nanny testNanny = nannyList.get(nannyList.size() - 1);
    }

    @Test
    @Transactional
    public void createNannyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = nannyRepository.findAll().size();

        // Create the Nanny with an existing ID
        nanny.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNannyMockMvc.perform(post("/api/nannies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nanny)))
            .andExpect(status().isBadRequest());

        // Validate the Nanny in the database
        List<Nanny> nannyList = nannyRepository.findAll();
        assertThat(nannyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllNannies() throws Exception {
        // Initialize the database
        nannyRepository.saveAndFlush(nanny);

        // Get all the nannyList
        restNannyMockMvc.perform(get("/api/nannies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(nanny.getId().intValue())));
    }

    @Test
    @Transactional
    public void getNanny() throws Exception {
        // Initialize the database
        nannyRepository.saveAndFlush(nanny);

        // Get the nanny
        restNannyMockMvc.perform(get("/api/nannies/{id}", nanny.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(nanny.getId().intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingNanny() throws Exception {
        // Get the nanny
        restNannyMockMvc.perform(get("/api/nannies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNanny() throws Exception {
        // Initialize the database
        nannyRepository.saveAndFlush(nanny);

        int databaseSizeBeforeUpdate = nannyRepository.findAll().size();

        // Update the nanny
        Nanny updatedNanny = nannyRepository.findById(nanny.getId()).get();
        // Disconnect from session so that the updates on updatedNanny are not directly saved in db
        em.detach(updatedNanny);

        restNannyMockMvc.perform(put("/api/nannies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNanny)))
            .andExpect(status().isOk());

        // Validate the Nanny in the database
        List<Nanny> nannyList = nannyRepository.findAll();
        assertThat(nannyList).hasSize(databaseSizeBeforeUpdate);
        Nanny testNanny = nannyList.get(nannyList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingNanny() throws Exception {
        int databaseSizeBeforeUpdate = nannyRepository.findAll().size();

        // Create the Nanny

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNannyMockMvc.perform(put("/api/nannies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(nanny)))
            .andExpect(status().isBadRequest());

        // Validate the Nanny in the database
        List<Nanny> nannyList = nannyRepository.findAll();
        assertThat(nannyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNanny() throws Exception {
        // Initialize the database
        nannyRepository.saveAndFlush(nanny);

        int databaseSizeBeforeDelete = nannyRepository.findAll().size();

        // Delete the nanny
        restNannyMockMvc.perform(delete("/api/nannies/{id}", nanny.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Nanny> nannyList = nannyRepository.findAll();
        assertThat(nannyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Nanny.class);
        Nanny nanny1 = new Nanny();
        nanny1.setId(1L);
        Nanny nanny2 = new Nanny();
        nanny2.setId(nanny1.getId());
        assertThat(nanny1).isEqualTo(nanny2);
        nanny2.setId(2L);
        assertThat(nanny1).isNotEqualTo(nanny2);
        nanny1.setId(null);
        assertThat(nanny1).isNotEqualTo(nanny2);
    }
}
