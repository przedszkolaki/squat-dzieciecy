package org.przedszkolaki.squat.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.przedszkolaki.squat.web.rest.TestUtil.createFormattingConversionService;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.przedszkolaki.squat.SquatApp;
import org.przedszkolaki.squat.domain.BillingCycle;
import org.przedszkolaki.squat.repository.BillingCycleRepository;
import org.przedszkolaki.squat.repository.DayRepository;
import org.przedszkolaki.squat.service.BillingCycleService;
import org.przedszkolaki.squat.web.rest.errors.ExceptionTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

/**
 * Test class for the BillingCycleResource REST controller.
 *
 * @see BillingCycleResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SquatApp.class)
public class BillingCycleResourceIntTest {

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_REGISTRATION_OPEN = false;
    private static final Boolean UPDATED_REGISTRATION_OPEN = true;

    @Autowired
    private BillingCycleRepository billingCycleRepository;

    @Autowired
	private BillingCycleService billingCycleService;

	@Autowired
	private DayRepository dayRepository;

	@Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restBillingCycleMockMvc;

    private BillingCycle billingCycle;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
		final BillingCycleResource billingCycleResource = new BillingCycleResource(billingCycleRepository,
				billingCycleService, dayRepository);
        this.restBillingCycleMockMvc = MockMvcBuilders.standaloneSetup(billingCycleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BillingCycle createEntity(EntityManager em) {
        BillingCycle billingCycle = new BillingCycle()
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .registrationOpen(DEFAULT_REGISTRATION_OPEN);
        return billingCycle;
    }

    @Before
    public void initTest() {
        billingCycle = createEntity(em);
    }

    @Test
    @Transactional
    public void createBillingCycle() throws Exception {
        int databaseSizeBeforeCreate = billingCycleRepository.findAll().size();

        // Create the BillingCycle
        restBillingCycleMockMvc.perform(post("/api/billing-cycles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(billingCycle)))
            .andExpect(status().isCreated());

        // Validate the BillingCycle in the database
        List<BillingCycle> billingCycleList = billingCycleRepository.findAll();
        assertThat(billingCycleList).hasSize(databaseSizeBeforeCreate + 1);
        BillingCycle testBillingCycle = billingCycleList.get(billingCycleList.size() - 1);
        assertThat(testBillingCycle.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testBillingCycle.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testBillingCycle.isRegistrationOpen()).isEqualTo(DEFAULT_REGISTRATION_OPEN);
    }

    @Test
    @Transactional
    public void createBillingCycleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = billingCycleRepository.findAll().size();

        // Create the BillingCycle with an existing ID
        billingCycle.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBillingCycleMockMvc.perform(post("/api/billing-cycles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(billingCycle)))
            .andExpect(status().isBadRequest());

        // Validate the BillingCycle in the database
        List<BillingCycle> billingCycleList = billingCycleRepository.findAll();
        assertThat(billingCycleList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStartDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = billingCycleRepository.findAll().size();
        // set the field null
        billingCycle.setStartDate(null);

        // Create the BillingCycle, which fails.

        restBillingCycleMockMvc.perform(post("/api/billing-cycles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(billingCycle)))
            .andExpect(status().isBadRequest());

        List<BillingCycle> billingCycleList = billingCycleRepository.findAll();
        assertThat(billingCycleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEndDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = billingCycleRepository.findAll().size();
        // set the field null
        billingCycle.setEndDate(null);

        // Create the BillingCycle, which fails.

        restBillingCycleMockMvc.perform(post("/api/billing-cycles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(billingCycle)))
            .andExpect(status().isBadRequest());

        List<BillingCycle> billingCycleList = billingCycleRepository.findAll();
        assertThat(billingCycleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRegistrationOpenIsRequired() throws Exception {
        int databaseSizeBeforeTest = billingCycleRepository.findAll().size();
        // set the field null
        billingCycle.setRegistrationOpen(null);

        // Create the BillingCycle, which fails.

        restBillingCycleMockMvc.perform(post("/api/billing-cycles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(billingCycle)))
            .andExpect(status().isBadRequest());

        List<BillingCycle> billingCycleList = billingCycleRepository.findAll();
        assertThat(billingCycleList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBillingCycles() throws Exception {
        // Initialize the database
        billingCycleRepository.saveAndFlush(billingCycle);

        // Get all the billingCycleList
        restBillingCycleMockMvc.perform(get("/api/billing-cycles?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(billingCycle.getId().intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].registrationOpen").value(hasItem(DEFAULT_REGISTRATION_OPEN.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getBillingCycle() throws Exception {
        // Initialize the database
        billingCycleRepository.saveAndFlush(billingCycle);

        // Get the billingCycle
        restBillingCycleMockMvc.perform(get("/api/billing-cycles/{id}", billingCycle.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(billingCycle.getId().intValue()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.registrationOpen").value(DEFAULT_REGISTRATION_OPEN.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingBillingCycle() throws Exception {
        // Get the billingCycle
        restBillingCycleMockMvc.perform(get("/api/billing-cycles/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBillingCycle() throws Exception {
        // Initialize the database
        billingCycleRepository.saveAndFlush(billingCycle);

        int databaseSizeBeforeUpdate = billingCycleRepository.findAll().size();

        // Update the billingCycle
        BillingCycle updatedBillingCycle = billingCycleRepository.findById(billingCycle.getId()).get();
        // Disconnect from session so that the updates on updatedBillingCycle are not directly saved in db
        em.detach(updatedBillingCycle);
        updatedBillingCycle
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .registrationOpen(UPDATED_REGISTRATION_OPEN);

        restBillingCycleMockMvc.perform(put("/api/billing-cycles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedBillingCycle)))
            .andExpect(status().isOk());

        // Validate the BillingCycle in the database
        List<BillingCycle> billingCycleList = billingCycleRepository.findAll();
        assertThat(billingCycleList).hasSize(databaseSizeBeforeUpdate);
        BillingCycle testBillingCycle = billingCycleList.get(billingCycleList.size() - 1);
        assertThat(testBillingCycle.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testBillingCycle.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testBillingCycle.isRegistrationOpen()).isEqualTo(UPDATED_REGISTRATION_OPEN);
    }

    @Test
    @Transactional
    public void updateNonExistingBillingCycle() throws Exception {
        int databaseSizeBeforeUpdate = billingCycleRepository.findAll().size();

        // Create the BillingCycle

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBillingCycleMockMvc.perform(put("/api/billing-cycles")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(billingCycle)))
            .andExpect(status().isBadRequest());

        // Validate the BillingCycle in the database
        List<BillingCycle> billingCycleList = billingCycleRepository.findAll();
        assertThat(billingCycleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBillingCycle() throws Exception {
        // Initialize the database
        billingCycleRepository.saveAndFlush(billingCycle);

        int databaseSizeBeforeDelete = billingCycleRepository.findAll().size();

        // Delete the billingCycle
        restBillingCycleMockMvc.perform(delete("/api/billing-cycles/{id}", billingCycle.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<BillingCycle> billingCycleList = billingCycleRepository.findAll();
        assertThat(billingCycleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BillingCycle.class);
        BillingCycle billingCycle1 = new BillingCycle();
        billingCycle1.setId(1L);
        BillingCycle billingCycle2 = new BillingCycle();
        billingCycle2.setId(billingCycle1.getId());
        assertThat(billingCycle1).isEqualTo(billingCycle2);
        billingCycle2.setId(2L);
        assertThat(billingCycle1).isNotEqualTo(billingCycle2);
        billingCycle1.setId(null);
        assertThat(billingCycle1).isNotEqualTo(billingCycle2);
    }
}
