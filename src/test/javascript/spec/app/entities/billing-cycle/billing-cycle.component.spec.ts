/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SquatTestModule } from '../../../test.module';
import { BillingCycleComponent } from 'app/entities/billing-cycle/billing-cycle.component';
import { BillingCycleService } from 'app/entities/billing-cycle/billing-cycle.service';
import { BillingCycle } from 'app/shared/model/billing-cycle.model';

describe('Component Tests', () => {
    describe('BillingCycle Management Component', () => {
        let comp: BillingCycleComponent;
        let fixture: ComponentFixture<BillingCycleComponent>;
        let service: BillingCycleService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SquatTestModule],
                declarations: [BillingCycleComponent],
                providers: []
            })
                .overrideTemplate(BillingCycleComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(BillingCycleComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BillingCycleService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new BillingCycle(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.billingCycles[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
