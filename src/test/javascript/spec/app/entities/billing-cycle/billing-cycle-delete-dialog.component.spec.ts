/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SquatTestModule } from '../../../test.module';
import { BillingCycleDeleteDialogComponent } from 'app/entities/billing-cycle/billing-cycle-delete-dialog.component';
import { BillingCycleService } from 'app/entities/billing-cycle/billing-cycle.service';

describe('Component Tests', () => {
    describe('BillingCycle Management Delete Component', () => {
        let comp: BillingCycleDeleteDialogComponent;
        let fixture: ComponentFixture<BillingCycleDeleteDialogComponent>;
        let service: BillingCycleService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SquatTestModule],
                declarations: [BillingCycleDeleteDialogComponent]
            })
                .overrideTemplate(BillingCycleDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BillingCycleDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BillingCycleService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
