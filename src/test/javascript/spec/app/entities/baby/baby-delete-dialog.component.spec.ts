/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { SquatTestModule } from '../../../test.module';
import { BabyDeleteDialogComponent } from 'app/entities/baby/baby-delete-dialog.component';
import { BabyService } from 'app/entities/baby/baby.service';

describe('Component Tests', () => {
    describe('Baby Management Delete Component', () => {
        let comp: BabyDeleteDialogComponent;
        let fixture: ComponentFixture<BabyDeleteDialogComponent>;
        let service: BabyService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SquatTestModule],
                declarations: [BabyDeleteDialogComponent]
            })
                .overrideTemplate(BabyDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BabyDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BabyService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
