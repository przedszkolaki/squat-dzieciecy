/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { SquatTestModule } from '../../../test.module';
import { BabyUpdateComponent } from 'app/entities/baby/baby-update.component';
import { BabyService } from 'app/entities/baby/baby.service';
import { Baby } from 'app/shared/model/baby.model';

describe('Component Tests', () => {
    describe('Baby Management Update Component', () => {
        let comp: BabyUpdateComponent;
        let fixture: ComponentFixture<BabyUpdateComponent>;
        let service: BabyService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SquatTestModule],
                declarations: [BabyUpdateComponent]
            })
                .overrideTemplate(BabyUpdateComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(BabyUpdateComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BabyService);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity', fakeAsync(() => {
                // GIVEN
                const entity = new Baby(123);
                spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.baby = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.update).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));

            it('Should call create service on save for new entity', fakeAsync(() => {
                // GIVEN
                const entity = new Baby();
                spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
                comp.baby = entity;
                // WHEN
                comp.save();
                tick(); // simulate async

                // THEN
                expect(service.create).toHaveBeenCalledWith(entity);
                expect(comp.isSaving).toEqual(false);
            }));
        });
    });
});
