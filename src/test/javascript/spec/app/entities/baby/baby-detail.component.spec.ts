/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { SquatTestModule } from '../../../test.module';
import { BabyDetailComponent } from 'app/entities/baby/baby-detail.component';
import { Baby } from 'app/shared/model/baby.model';

describe('Component Tests', () => {
    describe('Baby Management Detail Component', () => {
        let comp: BabyDetailComponent;
        let fixture: ComponentFixture<BabyDetailComponent>;
        const route = ({ data: of({ baby: new Baby(123) }) } as any) as ActivatedRoute;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SquatTestModule],
                declarations: [BabyDetailComponent],
                providers: [{ provide: ActivatedRoute, useValue: route }]
            })
                .overrideTemplate(BabyDetailComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(BabyDetailComponent);
            comp = fixture.componentInstance;
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(comp.baby).toEqual(jasmine.objectContaining({ id: 123 }));
            });
        });
    });
});
