/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { SquatTestModule } from '../../../test.module';
import { BabyComponent } from 'app/entities/baby/baby.component';
import { BabyService } from 'app/entities/baby/baby.service';
import { Baby } from 'app/shared/model/baby.model';

describe('Component Tests', () => {
    describe('Baby Management Component', () => {
        let comp: BabyComponent;
        let fixture: ComponentFixture<BabyComponent>;
        let service: BabyService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [SquatTestModule],
                declarations: [BabyComponent],
                providers: []
            })
                .overrideTemplate(BabyComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(BabyComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BabyService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Baby(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.babies[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
